import React from 'react';
import {View, Text} from 'react-native';

const DataFransiska = () => {
  return (
    <View>
      <View
        style={{
          backgroundColor: 'white',
          paddingLeft: 10,
          paddingRight: 30,
          marginLeft: 16,
          marginRight: 10,
          paddingTop: 20,
          paddingBottom: 20,
          marginTop: 20,
          borderRadius: 15,
          elevation: 5,
        }}>
        <Text
          style={{
            marginLeft: 10,
            fontSize: 25,
            fontWeight: 'bold',
            color: 'black',
          }}>
          Dr.Fransiska
        </Text>
        <Text style={{marginLeft: 10, fontSize: 16, marginTop: 20}}>
          Jumlah Pasien : 15/20
        </Text>
        <Text style={{marginLeft: 10, fontSize: 16}}>
          Jam Praktek : 07.00 - 08.00
        </Text>
      </View>
    </View>
  );
};

export default DataFransiska;
