import React from 'react';
import {View, Text, Image} from 'react-native';

const RiwayatBookingAlexandro = () => {
  return (
    <View>
      <View>
        <Text
          style={{
            marginLeft: 16,
            fontSize: 20,
            fontWeight: 'bold',
            color: 'black',
          }}>
          Prof.Alexandro
        </Text>
        <Text
          style={{
            marginLeft: 16,
            fontSize: 18,
            color: 'black',
          }}>
          Psikologi
        </Text>
        <Text
          style={{
            marginLeft: 16,
            fontSize: 20,
            marginTop: 20,
            color: 'black',
          }}>
          Jam : 10.00
        </Text>
        <Text
          style={{
            marginLeft: 16,
            fontSize: 20,
            fontWeight: 'bold',
            color: 'red',
          }}>
          Status : Selesai
        </Text>
      </View>
    </View>
  );
};

export default RiwayatBookingAlexandro;
