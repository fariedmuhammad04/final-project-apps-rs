import React from "react";
import { View, Text, TextInput, TouchableOpacity, Image, ScrollView } from "react-native";
import { useNavigation } from "@react-navigation/native";

const SignIn = () => {
  const navigation = useNavigation();
  return (
    <View style={{ flex: 1, backgroundColor: "#FFA9A1" }}>
      <Text
        style={{
          fontSize: 30,
          fontWeight: "bold",
          marginLeft: 16,
          marginTop: 10,
          color: "white",
        }}
      >
        Halo
      </Text>

      <View
        style={{
          alignItems: "center",
          marginTop: 90,
          flexDirection: "column",
          marginBottom: 140,
        }}
      >
        <Image source={require("./healthcarelogo.png")} style={{ width: 200, height: 200 }} />
        <Text
          style={{
            fontSize: 30,
            marginTop: 20,
            fontWeight: "bold",
            color: "white",
          }}
        >
          SanberCode
        </Text>
        <Text style={{ fontSize: 30, fontWeight: "bold", color: "white" }}>Medica</Text>
      </View>

      <View
        style={{
          flex: 1,
          backgroundColor: "#FF7366",
          borderTopRightRadius: 30,
          borderTopLeftRadius: 30,
        }}
      >
        <Text
          style={{
            marginLeft: 16,
            fontSize: 25,
            fontWeight: "bold",
            color: "white",
            marginTop: 20,
          }}
        >
          SignIn
        </Text>
        <TextInput
          style={{
            backgroundColor: "#FFFFFF",
            marginHorizontal: 20,
            borderRadius: 50,
            elevation: 5,
            marginTop: 30,
            width: 370,
            alignItems: "center",
            paddingVertical: 10,
            paddingLeft: 20,
          }}
          placeholder="Email"
          secureTextEntry={false}
        />
        <TextInput
          style={{
            backgroundColor: "#FFFFFF",
            marginHorizontal: 20,
            borderRadius: 50,
            elevation: 5,
            marginTop: 10,
            width: 370,
            alignItems: "center",
            paddingVertical: 10,
            paddingLeft: 20,
          }}
          placeholder="Password"
          secureTextEntry={false}
        />
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            marginTop: 25,
          }}
        >
          <TouchableOpacity onPress={() => navigation.navigate("SignUp")}>
            <Text style={{ marginLeft: 16, fontSize: 16, color: "blue" }}>Tidak Memiliki Akun?</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate("Home")}>
            <Text style={{ marginRight: 16, fontSize: 16, color: "white" }}>SignIn</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default SignIn;
