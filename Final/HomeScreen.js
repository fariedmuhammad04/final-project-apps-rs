import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import KategoriDokterAnak from './KategoriDokterAnak';
import KategoriDokterMata from './KategoriDokterMata';
import KategoriDokterJantung from './KategorDokterJantung';
import KategoriDokterPsikologi from './KategoriDokterPsikologi';
import KategoriDokterGigi from './KategoriDokterGigi';
import GabunganAnisa from './GabunganAnisa';
import GabunganAlexandro from './GabunganAlexandro';

const HomeScreen = () => {
  const navigation = useNavigation();

  return (
    <ScrollView>
      <View style={{flex: 1, backgroundColor: '#FFA9A1'}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginTop: 10,
          }}>
          <Text
            style={{
              fontSize: 25,
              fontWeight: 'bold',
              marginLeft: 16,
              color: 'white',
            }}>
            Halo, Faried
          </Text>
          <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
            <Image
              source={require('./profile.png')}
              style={{width: 65, height: 65, marginRight: 16}}
            />
          </TouchableOpacity>
        </View>

        <View>
          <TextInput
            style={{
              backgroundColor: '#FFFFFF',
              marginHorizontal: 20,
              borderRadius: 50,
              elevation: 5,
              marginTop: 20,
              width: 370,
              alignItems: 'center',
              paddingVertical: 10,
              paddingLeft: 20,
            }}
            placeholder="Search...."
            secureTextEntry={false}
          />
        </View>

        <View>
          <Text
            style={{
              fontSize: 25,
              fontWeight: 'bold',
              marginLeft: 16,
              marginTop: 20,
              color: 'white',
            }}>
            Kategori Dokter
          </Text>
          <ScrollView horizontal={true}>
            <View
              style={{
                marginTop: 20,
                flexDirection: 'row',
                alignItems: 'center',
                marginBottom: 10,
              }}>
              <TouchableOpacity onPress={() => navigation.navigate('Anak')}>
                <KategoriDokterAnak />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => navigation.navigate('Mata')}>
                <KategoriDokterMata />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => navigation.navigate('Jantung')}>
                <KategoriDokterJantung />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => navigation.navigate('Psikolog')}>
                <KategoriDokterPsikologi />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => navigation.navigate('Gigi')}>
                <KategoriDokterGigi />
              </TouchableOpacity>
            </View>
          </ScrollView>

          <View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                marginTop: 10,
              }}>
              <Text
                style={{
                  marginLeft: 16,
                  fontSize: 25,
                  fontWeight: 'bold',
                  color: 'white',
                }}>
                Riwayat Booking
              </Text>
              <TouchableOpacity onPress={() => navigation.navigate('ViewAll')}>
                <Text style={{marginRight: 16, color: 'black'}}>View All</Text>
              </TouchableOpacity>
            </View>
            <View>
              <GabunganAnisa />
              <GabunganAlexandro />
            </View>
          </View>

          <View style={{marginTop: 20, marginBottom: 20, alignItems: 'center'}}>
            <TouchableOpacity>
              <Text
                style={{
                  fontSize: 20,
                  backgroundColor: '#667EFC',
                  color: 'white',
                  paddingLeft: 60,
                  paddingRight: 60,
                  paddingTop: 10,
                  paddingBottom: 10,
                  borderRadius: 15,
                  elevation: 10,
                }}>
                Pesan Dokter
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default HomeScreen;
