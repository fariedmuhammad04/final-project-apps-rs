import React from "react";
import { View, Text, Image, StatusBar, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";

const Opening = () => {
  const navigation = useNavigation();

  return (
    <View style={{ flex: 1, backgroundColor: "#FFA9A1" }}>
      <StatusBar backgroundColor={"#FFA9A1"} barStyle={"dark-content"} />
      <View
        style={{
          alignItems: "center",
          marginTop: 200,
          flexDirection: "column",
          marginBottom: 90,
        }}
      >
        <Image source={require("./healthcarelogo.png")} style={{ width: 200, height: 200 }} />
        <Text
          style={{
            fontSize: 30,
            marginTop: 20,
            fontWeight: "bold",
            color: "white",
          }}
        >
          SanberCode
        </Text>
        <Text style={{ fontSize: 30, fontWeight: "bold", color: "white" }}>Medica</Text>
      </View>

      <View
        style={{
          flex: 1,
          backgroundColor: "#FF7366",
          borderTopRightRadius: 50,
          borderTopLeftRadius: 50,
          paddingHorizontal: 10,
          marginTop: 90,
        }}
      >
        <TouchableOpacity onPress={() => navigation.navigate("SignIn")}>
          <Text
            style={{
              backgroundColor: "#5777B4",
              fontSize: 20,
              marginHorizontal: 40,
              paddingVertical: 8,
              textAlign: "center",
              borderRadius: 15,
              marginTop: 30,
              color: "white",
              elevation: 10,
            }}
          >
            Login
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("SignUp")}>
          <Text
            style={{
              backgroundColor: "white",
              fontSize: 20,
              marginHorizontal: 40,
              paddingVertical: 8,
              textAlign: "center",
              borderRadius: 15,
              marginTop: 10,
              color: "#FF1818",
              elevation: 10,
            }}
          >
            Register
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Opening;
