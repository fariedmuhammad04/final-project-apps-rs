import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Opening from "./Opening";
import SignUp from "./SignUp";
import SignIn from "./SignIn";
import HomeScreen from "./HomeScreen";
import DokterAnakScreen from "./DokterAnakScreen";
import DetailDokterSteve from "./DetailDokterSteve";
import DetailDokterAlexa from "./DetailDokterAlexa";
import DetailDokterSinta from "./DetailDokterSinta";
import DetailDokterHobbin from "./DetailDokterHobbin";
import DetailDokterSanto from "./DetailDokterSanto";
import DetailDokterVica from "./DetailDokterVica";
import ProfileScreen from "./ProfileScreen";
import ViewAll from "./ViewAll";
import DokterMataScreen from "./DokterMataScreen";
import DokterJantungScreen from "./DokterJantungScreen";
import DokterPsikologScreen from "./DokterPsikologScreen";
import DokterGigiScreen from "./DokterGigiScreen";

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Opening" screenOptions={{ headerShown: false }}>
        <Stack.Screen name="Opening" component={Opening} />
        <Stack.Screen name="SignUp" component={SignUp} />
        <Stack.Screen name="SignIn" component={SignIn} />
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Anak" component={DokterAnakScreen} />
        <Stack.Screen name="Mata" component={DokterMataScreen} />
        <Stack.Screen name="Jantung" component={DokterJantungScreen} />
        <Stack.Screen name="Psikolog" component={DokterPsikologScreen} />
        <Stack.Screen name="Gigi" component={DokterGigiScreen} />
        <Stack.Screen name="DetailDokterSteve" component={DetailDokterSteve} />
        <Stack.Screen name="DetailDokterAlexa" component={DetailDokterAlexa} />
        <Stack.Screen name="DetailDokterSinta" component={DetailDokterSinta} />
        <Stack.Screen name="DetailDokterHobbin" component={DetailDokterHobbin} />
        <Stack.Screen name="DetailDokterSanto" component={DetailDokterSanto} />
        <Stack.Screen name="DetailDokterVica" component={DetailDokterVica} />
        <Stack.Screen name="Profile" component={ProfileScreen} />
        <Stack.Screen name="ViewAll" component={ViewAll} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
