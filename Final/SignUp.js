import React from "react";
import { View, Text, Image, TextInput, TouchableOpacity, StatusBar, ScrollView } from "react-native";
import { useNavigation } from "@react-navigation/native";

const SignUp = () => {
  const navigation = useNavigation();

  return (
    <View style={{ flex: 1, backgroundColor: "#FFA9A1" }}>
      <StatusBar backgroundColor={"#FFA9A1"} barStyle={"dark-content"} />
      <Text
        style={{
          fontSize: 25,
          marginLeft: 16,
          marginTop: 10,
          fontWeight: "bold",
          color: "white",
        }}
      >
        Selamat Datang
      </Text>

      <View
        style={{
          alignItems: "center",
          flexDirection: "column",
          marginTop: 70,
          marginBottom: 140,
        }}
      >
        <Image source={require("./healthcarelogo.png")} style={{ width: 150, height: 150 }} />
        <Text
          style={{
            fontSize: 20,
            fontWeight: "bold",
            marginTop: 20,
            color: "white",
          }}
        >
          SanberCode
        </Text>
        <Text style={{ fontSize: 20, fontWeight: "bold", color: "white" }}>Medica</Text>
      </View>

      <View
        style={{
          flex: 1,
          backgroundColor: "#FF7366",
          borderTopRightRadius: 30,
          borderTopLeftRadius: 30,
        }}
      >
        <View>
          <Text
            style={{
              fontSize: 25,
              fontWeight: "bold",
              marginLeft: 16,
              marginTop: 30,
              color: "white",
            }}
          >
            SignUp
          </Text>
          <TextInput
            style={{
              backgroundColor: "#FFFFFF",
              marginHorizontal: 20,
              borderRadius: 50,
              elevation: 5,
              marginTop: 20,
              width: 370,
              alignItems: "center",
              paddingVertical: 10,
              paddingLeft: 20,
            }}
            placeholder="Masukan Nama Anda"
            secureTextEntry={false}
          />
          <TextInput
            style={{
              backgroundColor: "#FFFFFF",
              marginHorizontal: 20,
              borderRadius: 50,
              elevation: 5,
              marginTop: 10,
              width: 370,
              alignItems: "center",
              paddingVertical: 10,
              paddingLeft: 20,
            }}
            placeholder="Email"
            secureTextEntry={false}
          />
          <TextInput
            style={{
              backgroundColor: "#FFFFFF",
              marginHorizontal: 20,
              borderRadius: 50,
              elevation: 5,
              marginTop: 10,
              width: 370,
              alignItems: "center",
              paddingVertical: 10,
              paddingLeft: 20,
            }}
            placeholder="No Hp"
            secureTextEntry={false}
          />
          <TextInput
            style={{
              backgroundColor: "#FFFFFF",
              marginHorizontal: 20,
              borderRadius: 50,
              elevation: 5,
              marginTop: 10,
              width: 370,
              alignItems: "center",
              paddingVertical: 10,
              paddingLeft: 20,
            }}
            placeholder="Password"
            secureTextEntry={true}
          />
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              marginTop: 29,
            }}
          >
            <TouchableOpacity onPress={() => navigation.navigate("SignIn")}>
              <Text style={{ marginLeft: 16, fontSize: 15, color: "blue" }}>Sudah Memiliki Akun?</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate("SignIn")}>
              <Text style={{ marginRight: 16, fontSize: 15, color: "white" }}>SignUp</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

export default SignUp;
