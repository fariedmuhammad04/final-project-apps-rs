import React from 'react';
import {View, Text} from 'react-native';

const DataUtomo = () => {
  return (
    <View>
      <View
        style={{
          backgroundColor: 'white',
          paddingLeft: 10,
          paddingRight: 30,
          marginRight: 16,
          paddingTop: 20,
          paddingBottom: 20,
          marginTop: 20,
          borderRadius: 15,
          elevation: 5,
        }}>
        <Text
          style={{
            marginLeft: 10,
            fontSize: 25,
            fontWeight: 'bold',
            color: 'black',
          }}>
          Dr.Utomo
        </Text>
        <Text style={{marginLeft: 10, fontSize: 16, marginTop: 20}}>
          Jumlah Pasien : 2/5
        </Text>
        <Text style={{marginLeft: 10, fontSize: 16}}>
          Jam Praktek : 12.00 - 12.30
        </Text>
      </View>
    </View>
  );
};

export default DataUtomo;
