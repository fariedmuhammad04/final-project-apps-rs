import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native';
import DataFransiska from './DataFransiska';
import DataUtomo from './DataUtomo';
import {useNavigation} from '@react-navigation/native';

const DokterPsikologScreen = () => {
  const navigation = useNavigation();

  return (
    <ScrollView>
      <View style={{flex: 1, backgroundColor: '#FFA9A1'}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginTop: 10,
          }}>
          <Text
            style={{
              fontSize: 25,
              marginLeft: 16,
              fontWeight: 'bold',
              color: 'white',
            }}>
            S.Psikolog
          </Text>
          <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
            <Image
              source={require('./profile.png')}
              style={{width: 65, height: 65, marginRight: 16}}
            />
          </TouchableOpacity>
        </View>
        <View>
          <TextInput
            style={{
              backgroundColor: '#FFFFFF',
              marginHorizontal: 20,
              borderRadius: 50,
              elevation: 5,
              marginTop: 20,
              width: 370,
              alignItems: 'center',
              paddingVertical: 10,
              paddingLeft: 20,
            }}
            placeholder="Search...."
            secureTextEntry={false}
          />
        </View>

        <View style={{marginTop: 20}}>
          <Text
            style={{
              marginLeft: 16,
              fontSize: 25,
              fontWeight: 'bold',
              color: 'white',
            }}>
            Praktek Hari Ini
          </Text>
          <ScrollView horizontal={true}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginBottom: 20,
              }}>
              <DataFransiska />
              <DataUtomo />
            </View>
          </ScrollView>

          <Text
            style={{
              marginLeft: 16,
              fontSize: 25,
              fontWeight: 'bold',
              color: 'white',
            }}>
            Dokter Lainnya
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 20,
              alignSelf: 'center',
            }}>
            <TouchableOpacity
              onPress={() => navigation.navigate('DetailDokterSteve')}>
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'center',
                  backgroundColor: 'white',
                  marginLeft: 16,
                  paddingTop: 30,
                  paddingBottom: 30,
                  marginRight: 16,
                  paddingRight: 30,
                  paddingLeft: 30,
                  borderRadius: 15,
                  elevation: 10,
                }}>
                <Image
                  source={require('./doktercowo.png')}
                  style={{width: 120, height: 120}}
                />
                <Text
                  style={{
                    marginTop: 20,
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: 'black',
                  }}>
                  Dr.Steve
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => navigation.navigate('DetailDokterAlexa')}>
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'center',
                  backgroundColor: 'white',
                  paddingTop: 30,
                  paddingBottom: 30,
                  marginRight: 16,
                  paddingRight: 30,
                  paddingLeft: 30,
                  borderRadius: 15,
                  elevation: 10,
                }}>
                <Image
                  source={require('./doktercewe.png')}
                  style={{width: 120, height: 120}}
                />
                <Text
                  style={{
                    marginTop: 20,
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: 'black',
                  }}>
                  Dr.Alexa
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 16,
              alignSelf: 'center',
            }}>
            <TouchableOpacity
              onPress={() => navigation.navigate('DetailDokterSinta')}>
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'center',
                  backgroundColor: 'white',
                  marginLeft: 16,
                  paddingTop: 30,
                  paddingBottom: 30,
                  marginRight: 16,
                  paddingRight: 30,
                  paddingLeft: 30,
                  borderRadius: 15,
                  elevation: 10,
                }}>
                <Image
                  source={require('./doktercewe.png')}
                  style={{width: 120, height: 120}}
                />
                <Text
                  style={{
                    marginTop: 20,
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: 'black',
                  }}>
                  Dr.Sinta
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => navigation.navigate('DetailDokterHobbin')}>
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'center',
                  backgroundColor: 'white',
                  paddingTop: 30,
                  paddingBottom: 30,
                  marginRight: 16,
                  paddingRight: 30,
                  paddingLeft: 30,
                  borderRadius: 15,
                  elevation: 10,
                }}>
                <Image
                  source={require('./doktercowo.png')}
                  style={{width: 120, height: 120}}
                />
                <Text
                  style={{
                    marginTop: 20,
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: 'black',
                  }}>
                  Dr.Hobbin
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 16,
              alignSelf: 'center',
              marginBottom: 20,
            }}>
            <TouchableOpacity
              onPress={() => navigation.navigate('DetailDokterSanto')}>
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'center',
                  backgroundColor: 'white',
                  marginLeft: 16,
                  paddingTop: 30,
                  paddingBottom: 30,
                  marginRight: 16,
                  paddingRight: 30,
                  paddingLeft: 30,
                  borderRadius: 15,
                  elevation: 10,
                }}>
                <Image
                  source={require('./doktercowo.png')}
                  style={{width: 120, height: 120}}
                />
                <Text
                  style={{
                    marginTop: 20,
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: 'black',
                  }}>
                  Dr.Santo
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => navigation.navigate('DetailDokterVica')}>
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'center',
                  backgroundColor: 'white',
                  paddingTop: 30,
                  paddingBottom: 30,
                  marginRight: 16,
                  paddingRight: 30,
                  paddingLeft: 30,
                  borderRadius: 15,
                  elevation: 10,
                }}>
                <Image
                  source={require('./doktercewe.png')}
                  style={{width: 120, height: 120}}
                />
                <Text
                  style={{
                    marginTop: 20,
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: 'black',
                  }}>
                  Dr.Vica
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default DokterPsikologScreen;
