import React from 'react';
import {View, Text, Image} from 'react-native';

const RiwayatBookingAnisa = () => {
  return (
    <View>
      <View>
        <Text
          style={{
            marginLeft: 16,
            fontSize: 20,
            fontWeight: 'bold',
            color: 'black',
          }}>
          Prof.Annisa Salma
        </Text>
        <Text
          style={{
            marginLeft: 16,
            fontSize: 18,
            color: 'black',
          }}>
          Specialist Mata
        </Text>
        <Text
          style={{
            marginLeft: 16,
            fontSize: 20,
            marginTop: 20,
            color: 'black',
          }}>
          Jam : 16.00
        </Text>
        <Text
          style={{
            marginLeft: 16,
            fontSize: 20,
            fontWeight: 'bold',
            color: 'red',
          }}>
          Status : Pengambilan Obat
        </Text>
      </View>
    </View>
  );
};

export default RiwayatBookingAnisa;
