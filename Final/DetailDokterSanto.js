import React from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const DetailDokterSanto = () => {
  const navigation = useNavigation();
  return (
    <View style={{flex: 1, backgroundColor: '#FFA9A1'}}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginTop: 10,
        }}>
        <TouchableOpacity onPress={() => navigation.navigate('Home')}>
          <Image
            source={require('./back.png')}
            style={{width: 50, height: 50, marginLeft: 16}}
          />
        </TouchableOpacity>
        <Image
          source={require('./profile.png')}
          style={{width: 65, height: 65, marginRight: 16}}
        />
      </View>
      <View
        style={{
          flexDirection: 'column',
          alignItems: 'center',
          marginTop: 10,
        }}>
        <Image
          source={require('./doktercowo.png')}
          style={{width: 170, height: 170}}
        />
        <Text
          style={{
            fontSize: 25,
            marginTop: 30,
            fontWeight: 'bold',
            color: 'black',
          }}>
          Dr.Santo
        </Text>
      </View>
      <View style={{marginTop: 10}}>
        <TextInput
          style={{
            backgroundColor: '#FFFFFF',
            marginHorizontal: 20,
            borderRadius: 25,
            elevation: 5,
            marginTop: 20,
            width: 370,
            alignItems: 'center',
            paddingVertical: 15,
            paddingLeft: 20,
          }}
          placeholder="Nama Lengkap..."
          secureTextEntry={false}
        />
        <TextInput
          style={{
            backgroundColor: '#FFFFFF',
            marginHorizontal: 20,
            borderRadius: 25,
            elevation: 5,
            marginTop: 20,
            width: 370,
            alignItems: 'center',
            paddingVertical: 15,
            paddingLeft: 20,
          }}
          placeholder="Masukan Keluhan..."
          secureTextEntry={false}
        />
      </View>

      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginTop: 30,
          alignSelf: 'center',
        }}>
        <Text
          style={{
            backgroundColor: 'white',
            paddingLeft: 40,
            paddingRight: 40,
            paddingTop: 15,
            paddingBottom: 15,
            marginRight: 50,
            borderRadius: 20,
            fontSize: 16,
            elevation: 5,
            color: 'black',
          }}>
          17.30
        </Text>
        <Text
          style={{
            backgroundColor: 'white',
            paddingLeft: 40,
            paddingRight: 40,
            paddingTop: 15,
            paddingBottom: 15,
            borderRadius: 20,
            fontSize: 16,
            elevation: 5,
            color: 'black',
          }}>
          20.00
        </Text>
      </View>
      <Text
        style={{
          alignSelf: 'center',
          marginTop: 30,
          fontSize: 18,
          backgroundColor: '#667EFC',
          color: 'white',
          paddingLeft: 40,
          paddingRight: 40,
          paddingTop: 10,
          paddingBottom: 10,
          borderRadius: 15,
        }}>
        Pesan Dokter
      </Text>
    </View>
  );
};

export default DetailDokterSanto;
