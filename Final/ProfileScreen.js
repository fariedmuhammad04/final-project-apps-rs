import React from 'react';
import {View, Text, Image, TextInput, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const ProfileScreen = () => {
  const navigation = useNavigation();
  return (
    <View style={{flex: 1, backgroundColor: '#FFA9A1'}}>
      <View style={{marginTop: 20, marginLeft: 16}}>
        <TouchableOpacity onPress={() => navigation.navigate('Home')}>
          <Image
            source={require('./back.png')}
            style={{width: 50, height: 50}}
          />
        </TouchableOpacity>
      </View>

      <View style={{flexDirection: 'column', alignItems: 'center'}}>
        <Image
          source={require('./profile.png')}
          style={{width: 160, height: 160}}
        />
        <Text
          style={{
            marginTop: 40,
            fontSize: 25,
            fontWeight: 'bold',
            color: '#0B5A42',
          }}>
          Patiens
        </Text>
        <Text
          style={{
            marginTop: 70,
            fontSize: 25,
            fontWeight: 'bold',
            color: 'white',
          }}>
          Muhammad Faried Gunawan
        </Text>
        <Text
          style={{
            marginTop: 20,
            fontSize: 25,
            fontWeight: 'bold',
            color: 'white',
          }}>
          0813-8921-4045
        </Text>
        <Text
          style={{
            marginTop: 20,
            fontSize: 25,
            fontWeight: 'bold',
            color: 'white',
          }}>
          11 September 2004
        </Text>
        <Text
          style={{
            marginTop: 20,
            fontSize: 25,
            fontWeight: 'bold',
            color: 'white',
          }}>
          Jakarta
        </Text>
      </View>
    </View>
  );
};

export default ProfileScreen;
