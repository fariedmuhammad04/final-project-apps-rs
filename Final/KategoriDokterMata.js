import React from 'react';
import {View, Text, Image} from 'react-native';

const KategoriDokterMata = () => {
  return (
    <View>
      <View
        style={{
          alignItems: 'center',
          marginRight: 10,
          backgroundColor: 'white',
          paddingHorizontal: 30,
          paddingVertical: 30,
          borderRadius: 10,
          elevation: 5,
        }}>
        <Image
          source={require('./eye.png')}
          style={{width: 70, height: 70, marginBottom: 10}}
        />
        <Text
          style={{
            color: 'black',
            fontWeight: 'bold',
            fontSize: 16,
            marginTop: 10,
          }}>
          Mata
        </Text>
      </View>
    </View>
  );
};

export default KategoriDokterMata;
