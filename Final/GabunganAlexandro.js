import React from 'react';
import {View, Text, Image} from 'react-native';
import RiwayatBookingAlexandro from './RiwayatBookingAlexandro';

const GabunganAlexandro = () => {
  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          backgroundColor: 'white',
          paddingTop: 20,
          paddingBottom: 20,
          marginLeft: 16,
          marginRight: 16,
          borderRadius: 20,
          marginTop: 10,
          elevation: 5,
        }}>
        <RiwayatBookingAlexandro />
        <Image
          source={require('./psikolog.png')}
          style={{width: 60, height: 60, marginLeft: 120}}
        />
      </View>
    </View>
  );
};

export default GabunganAlexandro;
