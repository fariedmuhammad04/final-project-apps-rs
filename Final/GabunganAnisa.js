import React from 'react';
import {View, Text, Image} from 'react-native';
import RiwayatBookingAnisa from './RiwayatBookingAnisa';

const GabunganAnisa = () => {
  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          backgroundColor: 'white',
          paddingTop: 20,
          paddingBottom: 20,
          marginLeft: 16,
          marginRight: 16,
          borderRadius: 20,
          marginTop: 20,
          elevation: 5,
        }}>
        <RiwayatBookingAnisa />
        <Image
          source={require('./eye.png')}
          style={{width: 60, height: 60, marginLeft: 20}}
        />
      </View>
    </View>
  );
};

export default GabunganAnisa;
