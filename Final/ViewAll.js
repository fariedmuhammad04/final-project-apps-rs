import React from 'react';
import {
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Text,
  ScrollView,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import GabunganAnisa from './GabunganAnisa';
import GabunganAlexandro from './GabunganAlexandro';

const ViewAll = () => {
  const navigation = useNavigation();
  return (
    <ScrollView>
      <View style={{flex: 1, backgroundColor: '#FFA9A1'}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginTop: 10,
          }}>
          <Text
            style={{
              fontSize: 25,
              fontWeight: 'bold',
              marginLeft: 16,
              color: 'white',
            }}>
            Booking List
          </Text>
          <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
            <Image
              source={require('./profile.png')}
              style={{width: 65, height: 65, marginRight: 16}}
            />
          </TouchableOpacity>
        </View>
        <View>
          <TextInput
            style={{
              backgroundColor: '#FFFFFF',
              marginHorizontal: 20,
              borderRadius: 50,
              elevation: 5,
              marginTop: 20,
              width: 370,
              alignItems: 'center',
              paddingVertical: 10,
              paddingLeft: 20,
            }}
            placeholder="Search...."
            secureTextEntry={false}
          />
        </View>
        <View>
          <GabunganAnisa />
          <GabunganAlexandro />
          <GabunganAlexandro />
          <GabunganAlexandro />
        </View>
        <TouchableOpacity>
          <Text
            style={{
              backgroundColor: '#667EFC',
              color: 'white',
              fontSize: 20,
              textAlign: 'center',
              marginTop: 30,
              paddingTop: 10,
              paddingBottom: 10,
              paddingLeft: 30,
              paddingRight: 30,
              marginRight: 100,
              marginLeft: 100,
              marginBottom: 20,
              borderRadius: 15,
              elevation: 10,
            }}>
            Hapus Booking
          </Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default ViewAll;
